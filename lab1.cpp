#include <iostream>
#include <memory>
#include <list>


struct Squad {
	enum fraction {
		MAGISTER,
		PALADIN,
		GODWOKEN

	};
	fraction fraction;
	int strength;
	int defence;
	int health;
	int dexterity;
	int dodging;
	enum FightType {
		MELEE,
		RANGED
	};
	FightType FightType;
};



int main() {

	std::list<std::shared_ptr<Squad>> squads = {
		std::make_shared<Squad>(Squad{Squad::fraction::GODWOKEN, 10, 13, 12, 11, 15, Squad::FightType::RANGED })
	};

	squads.push_back(std::make_shared<Squad>(Squad{ Squad::fraction::MAGISTER, 12, 12, 15, 11, 10, Squad::FightType::MELEE }));
	squads.push_front(std::make_shared<Squad>(Squad{ Squad::fraction::PALADIN, 14, 12, 15, 11, 11, Squad::FightType::MELEE }));

	for (const auto & obj : squads) {
		switch (obj->fraction)
		{
		case Squad::fraction::GODWOKEN:
			std::cout << "Fraction: Godwoken" << std::endl;
			break;
		case Squad::fraction::MAGISTER:
			std::cout << "Fraction: Magister" << std::endl;
			break;
		case Squad::fraction::PALADIN:
			std::cout << "Fraction: Paladin" << std::endl;
			break;
		}

		std::cout << "Strength: " << obj->strength << std::endl << "Defence: " << obj->defence << std::endl <<
			"Haelth: " << obj->health << std::endl << "Dexterity: " << obj->dexterity << std::endl << "Dodging: " << obj->dodging << std::endl;
		if (obj->FightType == Squad::FightType::MELEE) {
			std::cout << "Fight Type: Melee" << std::endl;
		}
		else {
			std::cout << "Fight Type: Ranged" << std::endl;
		}
		std::cout << "-------------------------------------------------------------------" << std::endl;
	}

	return 0;
}
